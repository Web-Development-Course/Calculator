let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
var expect = chai.expect;
var assert = chai.assert;


describe('Integration test - send http post request to web server', function() {
    it('should return {"display": "11"}', function(done) {
        let clientRequest = {
            calculatorState: {
                display: "1",
                currentOperator: null,
                left: null,
                right: false
            },
            input: "1"
        };
        chai.request('http://localhost:3000')
            .post('/calculate')
            .send(clientRequest)
            .end(function (err, res) {
                expect(res.body.display).to.equal('11');
                done();
            });
    });

    it('should return {"display": "13"}', function(done) {
        let clientRequest = {
            calculatorState: {
                display: "2",
                currentOperator: "+",
                left: "11",
                right: false
            },
            input: "="
        };
        chai.request('http://localhost:3000')
            .post('/calculate')
            .send(clientRequest)
            .end(function (err, res) {
                expect(res.body.display).to.equal('13');
                done();
            });
    });


});