let assert = require('assert');
let calculate = require('../routes/calculate');

describe('calculateNextState', function() {
    
    describe('check addition', function() {

        it('12+34= should display 55', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "+");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 55);
        });

        it('12+34=3 should display 3', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "+");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "=");
            s = calculate.calculateNextState(s, "3");
            assert.equal(s.display, 3);
        });
    
        it('12+34=3+4= should display 7', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "+");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "=");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "+");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 7);
        });    

    });

    describe('check subtraction', function() {
        it('155-55= should display 100', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "5");
            s = calculate.calculateNextState(s, "5");
            s = calculate.calculateNextState(s, "-");
            s = calculate.calculateNextState(s, "5");
            s = calculate.calculateNextState(s, "5");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 100);
        });
    });

    describe('check multiple', function() {
        it('12*12= should display 144', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "*");
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 144);
        });
    });

    describe('check division', function() {
        it('144/12= should display 12', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "/");
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 12);
        });
    });

    describe('check addition and multiple', function() {
        it('12+34=*10= should display 550', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "+");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "=");
            s = calculate.calculateNextState(s, "*");
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "0");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 550);
        });
    });

    describe('check change in the operator', function() {
        it('12*+34=*10= should display 550', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "*");
            s = calculate.calculateNextState(s, "+");
            s = calculate.calculateNextState(s, "4");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "=");
            s = calculate.calculateNextState(s, "*");
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "0");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 550);
        });
    });

    describe('check division by zero', function() {
        it('12/0= should display Cannot divide by zero', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "/");
            s = calculate.calculateNextState(s, "0");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 'Cannot divide by zero');
        });
    });

    describe('check using operator after division by zero', function() {
        it('12/0=* should display Cannot divide by zero', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "/");
            s = calculate.calculateNextState(s, "0");
            s = calculate.calculateNextState(s, "=");
            s = calculate.calculateNextState(s, "*");
            assert.equal(s.display, 'Cannot divide by zero');
        });
    });

    describe('check using operand after division by zero', function() {
        it('12/0=3= should display 3', function() {
            let s = null;
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "/");
            s = calculate.calculateNextState(s, "0");
            s = calculate.calculateNextState(s, "=");
            s = calculate.calculateNextState(s, "3");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 3);
        });
    });

    describe('check null situations', function() {
        it('should display 112', function() {
            let s = {display: 1};
            s = calculate.calculateNextState(s, "1");
            s = calculate.calculateNextState(s, "2");
            s = calculate.calculateNextState(s, "=");
            assert.equal(s.display, 112);
        });
    });

});