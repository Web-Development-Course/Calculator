/* jshint strict: true */
import {Router, NextFunction, Response, Request} from "express"

export interface IRoute{
    path: string;
    route: (req : Request, res: Response, next: NextFunction) => void;
}

export interface IClientState {
    left: string;
    right: boolean;
    currentOperator: string;
    display: string;
}

export interface ICalculateRouter {
    _router: Router;
    router: () => Router;
}

export interface IClientRequest extends Request {
    input: string;
    calculatorState: IClientState;
}

const ZERO_EXCEPTION = 'Cannot divide by zero';
const OPERATOR_EXCEPTION = 'invalid input';

export class CalculateRouter implements ICalculateRouter {
    _router: Router;

    constructor() {
        this._router = Router();
        this.init();
    }

    public get router() {
        return this._router;
    }

    private init() {
        let route: IRoute = {
            path: '/',
            route: function (req: Request, res: Response, next: NextFunction) {
                let query: IClientRequest = req.body;
                let response: IClientState = calculateNextState(query.calculatorState,
                                                                query.input);
                res.send(response);
            }
        }
        this._router.post(route.path, route.route);
    }
}


export function calculateNextState(jsonState: IClientState, input: string): IClientState {
    let oper = Number(input);
    if (!jsonState) {
        if (isNaN(oper)) {
            return null;
        }
        return {left: null, right: false, currentOperator: null, display: input};
    }
    if (isNaN(oper)) {
        // operator
        if (jsonState.currentOperator && 
            jsonState.currentOperator !== '=' && !jsonState.right) {
            try {
                const result = calculate(Number(jsonState.left),
                                         Number(jsonState.display),
                                         jsonState.currentOperator);
                jsonState.display = String(result);
            } catch (e) {
                jsonState.display = e.message;
            }
        }
        jsonState.currentOperator = input;        
        jsonState.left = jsonState.display;
        jsonState.right = true;
    } else {
        // number
        if (jsonState.currentOperator === '=' || 
            jsonState.display === ZERO_EXCEPTION ||
            jsonState.display === OPERATOR_EXCEPTION) {
            return {left: null, right: false, currentOperator: null, display: input};
        }
        if (jsonState.right) {
            jsonState.display = '';
            jsonState.right = false;
        }
        jsonState.display += input;
    }
    return jsonState;    
}

function calculate(left: number, right: number, operator: string): number {
    switch(operator) {
        case '*':
            return left * right;
        case '-':
            return left - right;
        case '+':
            return left + right;
        case '/':
            if (right === 0) {
                throw new Error(ZERO_EXCEPTION);
            }
            return left / right;
        default:
            throw new Error(OPERATOR_EXCEPTION);
    }
}
