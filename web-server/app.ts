let express = require('express');
let path = require('path');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let cors = require("cors");

import { CalculateRouter } from "./routes/calculate";

var app = express();

let calculate = new CalculateRouter();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(cors())
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/calculate", calculate.router);


// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err: any = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500).send(req.app.get('env') === 'development' ? err : GiveClientErrorMessage(err.name));
});

module.exports = app;

function GiveClientErrorMessage(errorMessageName: string){
  // Some switch case
}