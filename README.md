# Web Development Exercise
The application is implemented using JS and TypeScript + Mocha test runner for testing.

# Build
In order to build this project go to the web-server directory and run the following command:
 
```
npm run build
```

# Start the service
In order to start the web server service go to the web-server directory and run the command:
```
npm run start
```
**Note: you must build the project before starting the service**

# Test
In order to test the code go to the web-server directory and run the command:

```
npm run test
```
**Note: you must start the service before running the test**

# Docker
In order to run this app inside a container, run the following commands
inside the web-server directory:

```
sudo docker build -t calculator .
sudo docker -p 8080:3000 run calculator

```
In order to test the container navigate to localhost:8080/calculate

# Docker Compose
In order to run the whole app in containers using docker-compose, run the
following commands inside the web-server directory:

```
sudo docker-compose build
sudo docker-compose up

```
In order to test the container navigate to localhost:80

In order to stop the services run the following command:

```
sudo docker-compose stop

```

In order to start the services run the following command:

```
sudo docker-compose start

```